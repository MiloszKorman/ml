# --------------------------------------------------------------------------
# ------------  Metody Systemowe i Decyzyjne w Informatyce  ----------------
# --------------------------------------------------------------------------
#  Zadanie 1: Regresja liniowa
#  autorzy: A. Gonczarek, J. Kaczmar, S. Zareba
#  2017
# --------------------------------------------------------------------------

import numpy as np
from functools import reduce

from utils import polynomial


def mean_squared_error(x, y, w):
    """
    :param x: ciag wejsciowy Nx1
    :param y: ciag wyjsciowy Nx1
    :param w: parametry modelu (M+1)x1
    :return: blad sredniokwadratowy pomiedzy wyjsciami y
    oraz wyjsciami uzyskanymi z wielowamiu o parametrach w dla wejsc x
    """
    y_pred = polynomial(x, w)
    sum_sq_y_diff = sum(map(lambda el: (el[0] - el[1]) ** 2,
                            list(zip(y, y_pred))))[0]
    return sum_sq_y_diff / y.size


def design_matrix(x_train, M):
    """
    :param x_train: ciag treningowy Nx1
    :param M: stopien wielomianu 0,1,2,...
    :return: funkcja wylicza Design Matrix Nx(M+1) dla wielomianu rzedu M
    """
    return np.array([[(x_train[i] ** j)[0] for j in range(M + 1)]
                     for i in range(x_train.shape[0])])


def least_squares(x_train, y_train, M):
    """
    :param x_train: ciag treningowy wejscia Nx1
    :param y_train: ciag treningowy wyjscia Nx1
    :param M: rzad wielomianu
    :return: funkcja zwraca krotke (w,err), gdzie w sa parametrami dopasowanego wielomianu, a err blad sredniokwadratowy
    dopasowania
    """
    assert x_train.shape == y_train.shape
    fi = design_matrix(x_train, M)
    fi_t = fi.transpose()
    w = np.linalg.inv(fi_t @ fi) @ fi_t @ y_train
    err = mean_squared_error(x_train, y_train, w)
    return w, err


def regularized_least_squares(x_train, y_train, M, regularization_lambda):
    """
    :param x_train: ciag treningowy wejscia Nx1
    :param y_train: ciag treningowy wyjscia Nx1
    :param M: rzad wielomianu
    :param regularization_lambda: parametr regularyzacji
    :return: funkcja zwraca krotke (w,err), gdzie w sa parametrami dopasowanego wielomianu zgodnie z kryterium z regularyzacja l2,
    a err blad sredniokwadratowy dopasowania
    """
    assert x_train.shape == y_train.shape
    fi = design_matrix(x_train, M)
    fi_t = fi.transpose()
    I = np.eye(fi.shape[1])
    w = np.linalg.inv(fi_t @ fi + regularization_lambda * I) @ fi_t @ y_train
    err = mean_squared_error(x_train, y_train, w)
    return w, err


def model_selection(x_train, y_train, x_val, y_val, M_values):
    """
    :param x_train: ciag treningowy wejscia Nx1
    :param y_train: ciag treningowy wyjscia Nx1
    :param x_val: ciag walidacyjny wejscia Nx1
    :param y_val: ciag walidacyjny wyjscia Nx1
    :param M_values: tablica stopni wielomianu, ktore maja byc sprawdzone
    :return: funkcja zwraca krotke (w,train_err,val_err), gdzie w sa parametrami modelu, ktory najlepiej generalizuje dane,
    tj. daje najmniejszy blad na ciagu walidacyjnym, train_err i val_err to bledy na sredniokwadratowe na ciagach treningowym
    i walidacyjnym
    """
    assert x_train.shape == y_train.shape
    assert x_val.shape == y_val.shape
    all_r = [(w, err, mean_squared_error(x_val, y_val, w)) for w, err in
             [least_squares(x_train, y_train, M) for M in M_values]]
    return reduce(lambda acc, el: acc if acc[2] < el[2] else el, all_r)


def regularized_model_selection(x_train, y_train, x_val, y_val, M,
                                lambda_values):
    """
    :param x_train: ciag treningowy wejscia Nx1
    :param y_train: ciag treningowy wyjscia Nx1
    :param x_val: ciag walidacyjny wejscia Nx1
    :param y_val: ciag walidacyjny wyjscia Nx1
    :param M: stopien wielomianu
    :param lambda_values: lista ze wartosciami roznych parametrow regularyzacji
    :return: funkcja zwraca krotke (w,train_err,val_err,regularization_lambda), gdzie w sa parametrami modelu, ktory najlepiej generalizuje dane,
    tj. daje najmniejszy blad na ciagu walidacyjnym. Wielomian dopasowany jest wg kryterium z regularyzacja. train_err i val_err to
    bledy na sredniokwadratowe na ciagach treningowym i walidacyjnym. regularization_lambda to najlepsza wartosc parametru regularyzacji
    """
    assert x_train.shape == y_train.shape
    assert x_val.shape == y_val.shape
    all_r = [(w, err, mean_squared_error(x_val, y_val, w), l) for w, err, l in
             [(*regularized_least_squares(x_train, y_train, M, l), l) for l in
              lambda_values]]
    return reduce(lambda acc, el: acc if acc[2] < el[2] else el, all_r)
